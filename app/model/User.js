Ext.define('SimpleGrid.model.User', {
    extend: 'SimpleGrid.model.Base',
    idProperty: 'Id',
    fields: [
        {name: 'Id'},
        {name: 'Roles'},
        {name: 'UserId'},
        {name: 'FirstName'},
        {name: 'LastName'},
        {name: 'JobDescription'},
        {name: 'Phone'},
        {name: 'Email'},
        {name: 'GroupId'},
        {name: 'IsActive'},
        {name: 'CompanyId'},
        {name: 'IsAdmin'},
        {name: 'MasterId'}
    ],
    proxy: {
        type: 'rest',
        url: 'http://22prr.dyndns.org:8300/api/Users',
        cors: true,
        useDefaultXhrHeader: false,
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            allowSingle: true
        }
    }
})