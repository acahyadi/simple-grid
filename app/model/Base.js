Ext.define('SimpleGrid.model.Base', {
    extend: 'Ext.data.Model',
    schema: {
        namespace: 'SimpleGrid.model'
    }
})