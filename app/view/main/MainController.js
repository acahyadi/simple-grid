/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpleGrid.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onAdd: function() {
       var me = this,
           view = me.getView()
           grid = me.getReferences().usergrid;

       Ext.widget('userform',{
           _grid: grid
       }).show();
    },
    onSave: function() {
        var me = this,
            view = me.getView(),
            grid = view._grid,
            form = view.getReferences().formuser,
            store = grid.getStore(),
            values = form.getForm().getValues();

            if (view._record) {
                view._record.set(values);
                
                if (view._record.dirty == false) {
                    Ext.MessageBox.alert('Info', 'No Changed');
                    return;
                }
            } else {
                store.add(values);
            }
            
            view.setLoading(true);
            store.sync({
                success: function(batch, options) {
                    view.setLoading(false);
                    view.close();
                },
                failure: function() {
                    view.setLoading(false);
                    console.log('failure');
                }
            });
    },

    onEdit: function() {
        var me = this,
            view = me.getView(),
            grid = me.getReferences().usergrid,
            lastSelected = grid.getSelectionModel().getLastSelected();

        if (!lastSelected) {
            Ext.MessageBox.alert('Warning', 'Please select row');
            return false;
        }

        var form = Ext.widget('userform',{
            _grid: grid,
            _record: lastSelected
        });

        form.getReferences().formuser.getForm().setValues(lastSelected.data);

        form.show();

        form.getViewModel().set('title', 'Edit User');
    },
    onDelete: function() {
        var me = this,
            view = me.getView()
            grid = me.getReferences().usergrid,
            store = grid.getStore();

        var lastSelected = grid.getSelectionModel().getLastSelected();

        if (!lastSelected) {
            Ext.MessageBox.alert('Warning', 'Please select row');
            return false;
        }

        store.remove(lastSelected);

        store.sync();
    }
});
