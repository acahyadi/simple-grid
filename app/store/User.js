Ext.define('SimpleGrid.store.User', {
    extend: 'Ext.data.Store',
    model:'SimpleGrid.model.User',
    storeId: 'User',
    autoLoad: true, 
    remoteSort: false
});