Ext.define('SimpleGrid.view.main.UserForm', {
    extend: 'Ext.window.Window',
    xtype: 'userform',
    layout: 'fit',
    height: 400,
    width: 400, 
    modal: true,
    viewModel: {
        type: 'usermodel'
    },
    controller: 'main',
    bind: {
        title: '{title}'
    },
    items: {
        xtype: 'form',
        layout: 'form',
        reference: 'formuser',
        bodyPadding: 10, 
        defaults: {
            labelSeparator: '', 
            labelAlign: 'left'
        },
        items: [
            {
                xtype: 'textfield',
                name: 'FirstName',
                fieldLabel: 'First Name',
                allowBlank: false
            },{
                xtype: 'textfield',
                name: 'LastName',
                fieldLabel: 'last Name'               
            },{
                xtype: 'textfield',
                name: 'JobDescription',
                fieldLabel: 'Job Description'
            },{
                xtype: 'textfield',
                name: 'Phone',
                fieldLabel: 'Phone'
            },{
                xtype: 'textfield',
                name: 'Email',
                vtype: 'email',
                fieldLabel: 'Email'
            }
        ],
        buttons: [
            {
                text: 'Save',
                handler: 'onSave',
                formBind: true
            }
        ]
    }
});