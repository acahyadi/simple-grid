Ext.define('SimpleGrid.view.main.UserWindow',{
    extend: 'Ext.window.Window',
    requires: [
        'SimpleGrid.view.main.MainController',
        'Ext.grid.Panel',
        'SimpleGrid.view.main.UserModel',
        'SimpleGrid.view.main.UserForm'
    ],
    title:'Users',
    height: 500,
    width: 600,
    xtype:'userwindow',
    controller: 'main',
    layout: 'fit',
    tbar: [
        {
            text: 'Add',
            handler: 'onAdd'
        },{
            text:'Edit',
            handler: 'onEdit'
        },{
            text: 'Delete',
            handler: 'onDelete'
        }
    ],
    initComponent: function() {
        var me = this;
        me.items = me.createitems();
        me.callParent(arguments);
    },
    createitems: function() {

        var store = Ext.create('SimpleGrid.store.User');

        return {
            reference: 'usergrid',
            xtype: 'gridpanel',
            store: store,
            columns: [
                {text: 'First Name', dataIndex: 'FirstName', flex: 1, align: 'left'},
                {text: 'Last Name', dataIndex: 'LastName', flex: 1, align: 'left'},
                {text: 'Job Description', dataIndex: 'JobDescription', flex: 1, align: 'left'}
            ]
        };
    }
});