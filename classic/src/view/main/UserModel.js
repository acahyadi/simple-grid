Ext.define('SimpleGrid.view.main.UserModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.usermodel',
    data: {
        title: 'Add User',
        field: {
            FirstName: '',
            LastName: '',
            JobDescription: '',
            Phone: '',
            Email: ''
        }
    }
})